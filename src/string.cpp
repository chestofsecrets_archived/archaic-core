/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include <iostream>
#include <iterator>
#include <sstream>
#include <algorithm>
#include <regex>

#include "string.hpp"

using namespace Archaic;

std::vector<std::string> String::explode(const char &delimiter, const std::string &string)
{
    std::stringstream stream(string);
    std::string segment;
    std::vector<std::string> result;

    while (std::getline(stream, segment, delimiter)) {
        result.push_back(segment);
    }

    return result;
}

std::vector<std::string> String::explode(const std::string &delimiter, const std::string &string)
{
    size_t pos = 0, last = 0;
    std::string segment;
    std::vector<std::string> result;

    while ((pos = string.find(delimiter, last)) != std::string::npos) {
        result.push_back(string.substr(0, pos));
        last = pos + delimiter.length();
    }

    return result;
}

std::vector<std::string> String::explode(const std::function<Explosion(const char&)> &delimiter, const std::string &string, bool skipEmpty)
{
    std::vector<std::string> result;
    std::stringstream current;

    for (auto &ch : string) {
        auto res = delimiter(ch);

        switch (res) {
            case Explosion::Include:
                current << ch;
            case Explosion::Exclude:
                break;
            case Explosion::Next:
                current << ch;
            case Explosion::Break:
                {
                    auto str = current.str();
                    if (!(skipEmpty && str.empty())) {
                        result.push_back(current.str());
                    }
                    current.str(std::string());
                }
                break;
            case Explosion::Separate:
                {
                    auto str = current.str();
                    if (!(skipEmpty && str.empty())) {
                        result.push_back(current.str());
                    }
                    result.push_back(std::string(1, ch));
                    current.str(std::string());
                }
                break;
            case Explosion::Stop:
                {
                    auto str = current.str();
                    if (!(skipEmpty && str.empty())) {
                        result.push_back(current.str());
                    }
                }
                return result;
                break;
            case Explosion::Halt:
                return {};
        }
    }

    result.push_back(current.str());

    return result;
}

std::vector<std::string> String::explode(const std::string &string)
{
    std::vector<std::string> result;

    for (auto &letter : string) {
        result.emplace_back(std::string(1, letter));
    }

    return result;
}

std::string String::implode(const char &delimiter, const std::vector<std::string> &strings)
{
    if (strings.empty()) {
        return "";
    }
    std::ostringstream stream;

    std::copy(strings.begin(), strings.end() - 1, std::ostream_iterator<std::string>(stream, &delimiter));

    return stream.str() + *(strings.rbegin());
}

std::string String::implode(const std::string &delimiter, const std::vector<std::string> &strings)
{
    if (strings.empty()) {
        return "";
    }
    std::ostringstream stream;

    std::copy(strings.begin(), strings.end() - 1, std::ostream_iterator<std::string>(stream, delimiter.c_str()));

    return stream.str() + *(strings.rbegin());
}

std::string String::implode(const std::vector<std::string> &strings)
{
    std::ostringstream stream;

    std::copy(strings.begin(), strings.end(), std::ostream_iterator<std::string>(stream));

    return stream.str();
}

std::vector<std::string> String::preg_split(const std::string &pattern, const std::string &string, bool skipEmpty)
{
    std::regex regex(pattern);
    auto begin = std::sregex_iterator(string.begin(), string.end(), regex),
         end = std::sregex_iterator();
    std::vector<std::string> result;

    while (begin != end) {
        auto segment = (*begin++).str();
        if (!(skipEmpty && segment.empty())) {
            result.push_back(segment);
        }
    }

    return result;
}

std::string String::trim(const std::string &string)
{
    auto result = string;

    result.erase(result.begin(), std::find_if_not(result.begin(), result.end(), [] (int character) {
        return std::isspace(character);
    }));

    result.erase(std::find_if_not(result.rbegin(), result.rend(), [] (int character) {
        return std::isspace(character);
    }).base(), result.end());

    return result;
}
