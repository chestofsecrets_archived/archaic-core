/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <iostream>
#include <algorithm>
#include <sstream>

#include "event-object.hpp"

using namespace Archaic;

EventObject::EventObject(): GuardObject() {}

EventObject* EventObject::on(const std::string &eventName, const Job &listener, const std::string &groupName)
{
    return this->guard<EventObject*>([this, &eventName, &listener, &groupName] () {
        auto typePosition = this->eventTypes.find(eventName);
        if (typePosition == this->eventTypes.end()) {
            JobType map;

            map.emplace(eventName, JobGroup({listener}));

            this->eventTypes.emplace(eventName, map);
        } else {
            auto &group = typePosition->second;
            auto groupPosition = group.find(groupName);

            if (groupPosition == group.end()) {
                group.emplace(groupName, JobGroup({listener}));
            } else {
                groupPosition->second.push_back(listener);
            }
        }
        return this;
    });
}

std::string EventObject::once(const std::string &eventName, const Job &listener, const std::string &groupName)
{
    return this->guard<std::string>([this, &eventName, &listener, &groupName] () {
        std::stringstream stream;
        stream << groupName << &listener;
        std::string uniqueName = stream.str();

        auto actualListener = [listener, this, eventName, uniqueName]() {
            listener();
            this->off(eventName, uniqueName);
            return JobResult::NextGroup;
        };

        auto typePosition = this->eventTypes.find(eventName);
        if (typePosition == this->eventTypes.end()) {
            JobType map;

            map.emplace(uniqueName, JobGroup({actualListener}));

            this->eventTypes.emplace(eventName, map);
        } else {
            auto &type = typePosition->second;
            auto groupPosition = type.find(uniqueName);

            if (groupPosition == type.end()) {
                type.emplace(uniqueName, JobGroup({actualListener}));
            } else {
                groupPosition->second.push_back(listener);
            }
        }

        return uniqueName;
    });
}

EventObject* EventObject::off(const std::string &eventName, const std::string &groupName)
{
    return this->guard<EventObject*>([this, &eventName, &groupName] () {
        auto typePosition = this->eventTypes.find(eventName);

        if (typePosition == this->eventTypes.end()) {
            return this;
        }

        if (groupName.empty()) {
            this->eventTypes[eventName].clear();
            return this;
        }

        auto &group = typePosition->second;
        auto groupPosition = group.find(groupName);

        if (groupPosition == group.end()) {
            return this;
        }
        groupPosition->second.clear();

        return this;
    });
}

unsigned long EventObject::listeners(const std::string &eventName, const std::string &groupName)
{
    return this->guard<unsigned long>([this, &eventName, &groupName] () {
        unsigned long count = 0;

        bool emptyEvent = eventName.empty(),
             emptyGroup = groupName.empty();

        for (auto &typesIterator : this->eventTypes) {
            if (!emptyEvent) {
                bool match = typesIterator.first == eventName;
                if (!match) {
                    continue;
                }
                for (auto &groupsIterator : typesIterator.second) {
                    if (emptyGroup || groupsIterator.first == groupName) {
                        count += groupsIterator.second.size();
                    }
                }
                if (match) {
                    return count;
                }
            } else {
                for (auto &groupsIterator : typesIterator.second) {
                    if (emptyGroup || groupsIterator.first == groupName) {
                        count += groupsIterator.second.size();
                    }
                }
            }
        }

        return count;
    });
}

bool EventObject::trigger(const std::string &eventName)
{
    return this->guard<bool>([this, eventName] () {
        auto typePosition = this->eventTypes.find(eventName);
        if (typePosition == this->eventTypes.end()) {
            return true;
        }

        for (auto groupsIterator = typePosition->second.rbegin(); groupsIterator != typePosition->second.rend(); groupsIterator++) {
            for (auto listenerIterator = groupsIterator->second.rbegin(); listenerIterator != groupsIterator->second.rend(); listenerIterator++) {
                switch (this->loose<JobResult>(Job(*listenerIterator))) {
                    case JobResult::NextGroup:
                        goto Next;
                    case JobResult::Stop:
                        return false;
                    case JobResult::Continue:
                        continue;
                }
            }
            Next: continue;
        }
        return true;
    });
}

std::string EventObject::message(const std::string &eventName, const std::string &groupName)
{
    return this->once("message", [this, eventName] () {
        return this->trigger(eventName) ? JobResult::NextGroup : JobResult::Stop;
    }, groupName);
}

std::string EventObject::message(const Job &job, const std::string &groupName)
{
    return this->once("message", job, groupName);
}

EventObject* EventObject::shush(const std::string &label)
{
    return this->off("message", label);
}

bool EventObject::listen()
{
    return this->trigger("message");
}
