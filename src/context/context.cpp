/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "engine.hpp"

#include "context/context.hpp"

using namespace Archaic;

std::map<std::string, std::function<ContextElement*()>> Context::builders;

Context::Context(): ContextElement("context") {}

Context::Context(Context *context): ContextElement(context)
{

}

ContextElement* Context::build(const std::string &name)
{
    auto pos = Context::builders.find(name);

    if (pos == Context::builders.end()) {
        Engine::get()->logger()->warn("Could not find a builder for element of type \"" + name + "\".");
        return new ContextElement(name);
    }

    return pos->second()->type(name);
}