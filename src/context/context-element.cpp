/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "context/context.hpp"

using namespace Archaic;

ContextElement::ContextElement(const std::string &type)
{
    this->__type = type;
}

ContextElement::ContextElement(ContextElement* element, bool passEvents)
{
    this->__type = element->__type;
    this->__id = element->__id;
    this->classList = element->classList;
    this->__children.reserve(element->__children.size());
    if (passEvents) {
        this->eventTypes = element->eventTypes;
    }

    for (auto &child : element->__children) {
        auto clone = Context::build(child->__type);
        clone->__parent = this;
        if (passEvents) {
            clone->eventTypes = child->eventTypes;
        }
        this->__children.push_back(clone);
    }
}

ContextElement::ContextElement(ContextElement* element): ContextElement(element, false) {}

ContextElement* ContextElement::parent()
{
    return this->__parent;
}

std::vector<ContextElement*> ContextElement::parents()
{
    std::vector<ContextElement*> parents;
    auto element = this->__parent;

    while (element) {
        parents.push_back(element);

        element = element->__parent;
    }

    return parents;
}

std::vector<ContextElement*> ContextElement::children(bool deep)
{
    if (!deep) {
        return this->__children;
    }

    auto children = this->__children;

    for (auto child : this->__children) {
        auto deepChildren = child->children(true);
        children.insert(children.end(), deepChildren.begin(), deepChildren.end());
    }

    return children;
}

ContextElement* ContextElement::append(ContextElement* child)
{
    child->__parent = this;
    this->__children.push_back(child);

    return this;
}

std::string ContextElement::id()
{
    return this->__id;
}

ContextElement* ContextElement::id(const std::string &id)
{
    this->__id = id;

    return this;
}

std::string ContextElement::type()
{
    return this->__type;
}

ContextElement* ContextElement::type(const std::string &type) {
    this->__type = type;

    return this;
}

bool ContextElement::hasClass(const std::string &className)
{
    return std::find(this->classList.begin(), this->classList.end(), className) != this->classList.end();
}

ContextElement* ContextElement::addClass(const std::string &className)
{
    if (this->hasClass(className)) {
        return this;
    }

    this->classList.push_back(className);

    return this;
}

ContextElement* ContextElement::removeClass(const std::string &className)
{
    auto position = std::find(this->classList.begin(), this->classList.end(), className);

    if (position == this->classList.end()) {
        return this;
    }

    this->classList.erase(position);

    return this;
}

std::vector<std::string> ContextElement::getClasses()
{
    return this->classList;
}

ContextElement::~ContextElement()
{
    for (auto &child : this->__children) {
        delete child;
    }
}
