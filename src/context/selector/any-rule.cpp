/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "context/selector/any-rule.hpp"

using namespace Archaic::Selector;

AnyRule::AnyRule(const std::vector<std::string> &args): Rule(args)
{
    this->expressions.reserve(args.size());

    for (auto &arg : args) {
        this->expressions.push_back(new Expression(arg));
    }
}

std::vector<ContextElement*> AnyRule::test(ContextElement *element, bool inverted)
{
    if (this->expressions.empty()) {
        return {element};
    }

    for (auto expression : this->expressions) {
        auto result = expression->exec(element);

        if ((bool)(result.size()) != inverted) {
            return {element};
        }
    }
    return {};
}


AnyRule::~AnyRule()
{
    for (auto &expression : this->expressions) {
        delete expression;
    }
}