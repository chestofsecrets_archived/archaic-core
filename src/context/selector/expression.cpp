/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <iostream>
#include <sstream>

#include "string.hpp"
#include "activity.hpp"
#include "context/selector/rule.hpp"

#include "context/selector/expression.hpp"

using namespace Archaic::Selector;

const char Expression::Operator::DeepFlag = '~';
const char Expression::Operator::Child = '>';
const char Expression::Operator::Parent = '<';
const char Expression::Operator::Class = '.';
const char Expression::Operator::Id = '#';
const char Expression::Operator::Rule = ':';
const char Expression::Operator::InvertFlag = '!';
const char Expression::Operator::StatementSeparator = ',';
const char Expression::Operator::ArgumentSeparator = ';';
const char Expression::Operator::ListBegin = '[';
const char Expression::Operator::ListEnd = ']';
const char Expression::Operator::Any = '*';

Expression::Expression(const std::string &expression)
{
    unsigned level = 0;
    auto currentStatement = new Statement();
    std::vector<std::pair<char, std::string>> currentIdentifier;
    std::vector<std::string> rulesArguments;
    char identifierOperator = '\0', attributeOperator = '\0';
    std::string ruleName;
    std::stringstream attributeStream, argumentStream;

    auto fnResetArgument = [&rulesArguments, &attributeStream] () {
        rulesArguments.push_back(attributeStream.str());
        attributeStream.str(std::string());
    };

    auto fnResetRuleName = [&ruleName, &attributeStream] () {
        ruleName = attributeStream.str();
    };

    unsigned long ruleIndex = 0;

    auto fnPushRule = [&currentStatement, &rulesArguments, &ruleName, &ruleIndex] () {
        auto rule = Rule::create(ruleName, rulesArguments);

        if (rule) {
            currentStatement->rules.emplace(ruleIndex++, rule);
        }
        rulesArguments.clear();
    };

    auto fnResetAttribute = [&currentIdentifier, &attributeStream, &attributeOperator] (const char &to, bool allowEmpty = false) {
        auto attribute = attributeStream.str();

        if (allowEmpty || !attribute.empty()) {
            currentIdentifier.emplace_back(std::pair<char, std::string>(attributeOperator, attribute));
            attributeStream.str(std::string());
        }

        attributeOperator = to;
    };

    auto fnResetIdentifier = [&currentStatement, &currentIdentifier, &identifierOperator] (const char &to, bool allowEmpty = false) {
        if (allowEmpty || !currentIdentifier.empty()) {
            currentStatement->statement.emplace_back(std::pair<char, std::vector<std::pair<char, std::string>>>(identifierOperator, currentIdentifier));
            currentIdentifier.clear();
        }
        identifierOperator = to;
    };

    auto fnResetStatement = [this, &currentStatement] () {
        if (currentStatement->statement.empty()) {
            delete currentStatement;
        } else {
            this->statements.push_back(currentStatement);
        }

        currentStatement = new Statement();
    };

    bool ruleNameFlag = false;

    for (auto &ch : expression) {
        if (level > 0) {
            switch (ch) {
                case Operator::ListBegin:
                    level++;
                    break;
                case Operator::ListEnd:
                    if (--level == 0) {
                        fnResetArgument();
                        fnPushRule();
                        ruleNameFlag = false;
                        continue;
                    }
                    break;
                case Operator::ArgumentSeparator:
                    if (level == 1) {
                        fnResetArgument();
                        continue;
                    }
                    break;
                default:
                    attributeStream << ch;
                    continue;
            }
        }
        switch (ch) {
            case Operator::ListBegin:
                if (!ruleNameFlag) {
                    fnResetAttribute(Operator::Rule);
                    ruleNameFlag = true;
                }
                fnResetRuleName();
                fnResetAttribute('\0', true);
                level++;
                break;
            case Operator::ListEnd:
                continue;
            case '\n':
            case '\r':
            case '\t':
            case '\0':
            case ' ':
            case Operator::Any:
                if (ruleNameFlag) {
                    fnResetRuleName();
                    fnPushRule();
                } else {
                    fnResetAttribute('\0');
                }
                break;
            case Operator::ArgumentSeparator:
            case Operator::StatementSeparator:
                if (ruleNameFlag) {
                    fnResetRuleName();
                    fnPushRule();
                    ruleNameFlag = false;
                } else {
                    fnResetAttribute('\0');
                }
                fnResetIdentifier('\0', true);
                fnResetStatement();
                ruleIndex = 0;
                break;
            case Operator::Id:
            case Operator::Class:
                if (ruleNameFlag) {
                    fnResetRuleName();
                    fnPushRule();
                    ruleNameFlag = false;
                } else {
                    fnResetAttribute(ch);
                }
                break;
            case Operator::Rule:
                if (ruleNameFlag) {
                    fnResetRuleName();
                    fnPushRule();
                } else {
                    fnResetAttribute(ch);
                    ruleNameFlag = true;
                }
                break;
            case Operator::InvertFlag:
                if (ruleNameFlag) {
                    fnResetRuleName();
                    fnPushRule();
                    ruleNameFlag = false;
                } else {
                    fnResetAttribute(ch);
                }
                fnResetAttribute('\0', true);
                break;
            case Operator::Child:
            case Operator::Parent:
            case Operator::DeepFlag:
                if (ruleNameFlag) {
                    fnResetRuleName();
                    fnPushRule();
                    ruleNameFlag = false;
                } else {
                    fnResetAttribute('\0');
                }
                fnResetIdentifier(ch, true);
                break;
            default:
                attributeStream << ch;
        }
    }
    if (ruleNameFlag) {
        fnResetRuleName();
        fnPushRule();
    }
    auto attribute = attributeStream.str();
    if (!attribute.empty()) {
        currentIdentifier.emplace_back(std::pair<char, std::string>(attributeOperator, attribute));
    }

    currentStatement->statement.emplace_back(std::pair<char, std::vector<std::pair<char, std::string>>>(identifierOperator, currentIdentifier));
    this->statements.push_back(currentStatement);
}

std::vector<ContextElement*> Expression::exec(ContextElement *element){
    std::vector<ContextElement*> result;

    for (auto &statement : this->statements) {
        std::vector<ContextElement*> toCheck = {element};

        bool deepFlag = false;

        size_t ruleIndex = 0;

        for (auto &identifierPair : statement->statement) {
            bool invertFlag = false;

            switch (identifierPair.first) {
                case Operator::DeepFlag:
                    deepFlag = !deepFlag;
                    if (identifierPair.second.empty()) {
                        continue;
                    }
                    break;
                case Operator::Child:
                    {
                        decltype(toCheck) newToCheck;
                        for (auto &parent : toCheck) {
                            auto children = parent->children(deepFlag);
                            newToCheck.insert(newToCheck.end(), children.begin(), children.end());
                        }
                        toCheck = newToCheck;
                    }
                    break;
                case Operator::Parent:
                    {
                        decltype(toCheck) newToCheck;
                        if (deepFlag) {
                            for (auto &child : toCheck) {
                                    auto parents = child->parents();
                                    newToCheck.insert(newToCheck.end(), parents.begin(), parents.end());
                            }
                        } else {
                            for (auto &child : toCheck) {
                                auto parent = child->parent();
                                newToCheck.push_back(parent);
                            }
                        }
                        newToCheck.erase(std::unique(newToCheck.begin(), newToCheck.end()), newToCheck.end());
                        toCheck = newToCheck;
                    }
                    break;
            }

            for (auto &attributePair : identifierPair.second) {

                switch (attributePair.first) {
                    case '\0':
                    {
                        decltype(toCheck) newToCheck;
                        for (auto &item : toCheck) {
                            if ((item->type() == attributePair.second) != invertFlag) {
                                newToCheck.push_back(item);
                            }
                        }
                        toCheck = newToCheck;
                    }
                        break;
                    case Operator::InvertFlag:
                        invertFlag = !invertFlag;
                        if (attributePair.second.empty()) {
                            continue;
                        }
                        break;
                    case Operator::Id:
                        for (auto &item : toCheck) {
                            if ((item->id() == attributePair.second) != invertFlag) {
                                toCheck = {item};

                                goto SetResult;
                            }
                        }
                        return {};
                    case Operator::Class:
                        {
                            decltype(toCheck) newToCheck;
                            for (auto &item : toCheck) {
                                if (item->hasClass(attributePair.second) != invertFlag) {
                                    newToCheck.push_back(item);
                                }
                            }
                            toCheck = newToCheck;
                        }
                        break;
                    case Operator::Rule:
                        {
                            auto pos = statement->rules.find(ruleIndex++);

                            if (pos == statement->rules.end()) {
                                break;
                            }

                            auto &rule = pos->second;

                            decltype(toCheck) newToCheck;
                            for (auto &item : toCheck) {
                                auto ruleResult = rule->test(item, invertFlag);

                                if (ruleResult.empty()) {
                                    continue;
                                }

                                newToCheck.insert(newToCheck.end(), ruleResult.begin(), ruleResult.end());
                            }

                            newToCheck.erase(std::unique(newToCheck.begin(), newToCheck.end()), newToCheck.end());

                            toCheck = newToCheck;
                        }
                        break;
                }

                invertFlag = false;
            }

            deepFlag = false;
        }

        SetResult: result.insert(result.end(), toCheck.begin(), toCheck.end());
    }

    result.erase(std::unique(result.begin(), result.end()), result.end());

    return result;
}

Expression::~Expression()
{
    for (auto &statement : this->statements) {
        for (auto &rulePair : statement->rules) {
            delete rulePair.second;
        }
        delete statement;
    }
}
