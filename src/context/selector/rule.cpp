/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "context/selector/expression.hpp"

#include "context/selector/rule.hpp"

using namespace Archaic::Selector;

std::map<std::string, std::function<Rule*(const std::vector<std::string>&)>> Rule::rules;

Rule::Rule(const std::vector<std::string> &args) {}

bool Rule::cancel(const std::string &ruleName)
{
    auto position = Rule::rules.find(ruleName);

    if (position == Rule::rules.end()) {
        return false;
    }

    Rule::rules.erase(position);

    return true;
}

Rule* Rule::create(const std::string &ruleName, const std::vector<std::string> &args)
{
    auto position = Rule::rules.find(ruleName);

    return position == Rule::rules.end() ? nullptr : position->second(args);
}