/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "context/selector/combine-rule.hpp"

using namespace Archaic::Selector;

CombineRule::CombineRule(const std::vector<std::string> &args): Rule(args)
{
    this->expressions.reserve(args.size());

    for (auto &arg : args) {
        this->expressions.push_back(new Expression(arg));
    }
}

std::vector<ContextElement*> CombineRule::test(ContextElement *element, bool inverted)
{
    std::vector<ContextElement*> result = {element};

    for (auto &expr : expressions) {
        auto exprResult = expr->exec(element);

        result.insert(result.end(), exprResult.begin(), exprResult.end());
    }

    result.erase(std::unique(result.begin(), result.end()), result.end());

    return result;
}


CombineRule::~CombineRule()
{
    for (auto &expression : this->expressions) {
        delete expression;
    }
}