/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <string>
#include <vector>

#include "object.hpp"

using namespace Archaic;

std::string Object::toBinary()
{
    return "o";
}

std::string Object::toBinary(const std::vector<std::string> &params)
{
    return "o";
}

void Object::fromBinary(const std::map<std::string, std::any> &params) {}