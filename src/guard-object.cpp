/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "guard-object.hpp"

using namespace Archaic;

GuardObject::GuardObject(): Object() {}

void GuardObject::vguard(const std::function<void()> &func)
{
    this->mutex.lock();
    func();
    this->mutex.unlock();
}

void GuardObject::vloose(const std::function<void()> &func)
{
    this->mutex.unlock();
    func();
    this->mutex.lock();
}