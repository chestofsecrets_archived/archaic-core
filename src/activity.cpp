/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <iostream>

#include "task/async-loop.hpp"
#include "context/context.hpp"

#include "activity.hpp"

using namespace Archaic;

Activity::Activity()
{
    this->on("start", [this] () {
        this->started = true;

        return JobResult::Continue;
    })->on("stop", [this] () {
        this->started = false;

        return JobResult::Continue;
    });
}

Thread* Activity::async(Thread *task)
{
    this->vguard([this, &task] () {
        this->threads.push_back(task);
    });

    if (started) {
        task->trigger("start");
    }

    return task;
}

Thread* Activity::async(const Job& func, bool loop)
{
    if (loop) {
        return this->async(new AsyncLoop(this, func));
    } else {
        return this->async(new Thread(this, func));
    }
}

Activity* Activity::sync(Thread *thread)
{
    this->message([this, thread] () {
        thread->trigger("sync");
        thread->join();

        auto position = std::find(this->threads.begin(), this->threads.end(), thread);

        if (position == this->threads.end()) {
            return JobResult::Continue;
        }

        this->threads.erase(position);
        delete thread;

        return JobResult::Continue;
    });

    return this;
}

bool Activity::start()
{
    auto engine = Engine::get();

    if (!engine) {
        Debug::Logger(new Debug::LogProcessor).fatal("Engine was not initialized");
        return false;
    }

    if (!this->trigger("start")) {
        return false;
    }

    engine->addActivity(this);

    for (auto &thread : this->threads) {
        thread->trigger("start");
    }

    do {
        this->listen();
    } while (this->started && this->trigger("execute"));

    for (auto &thread : this->threads) {
        thread->trigger("stop");
    }

    this->threads.clear();

    engine->removeActivity(this);

    return this->trigger("stop");
}

Activity* Activity::stop()
{
    this->message("stop");

    return this;
}

Activity* Activity::name(const std::string &name)
{
    this->__name = name;

    return this;
}

std::string& Activity::name()
{
    return this->__name;
}

Activity* Activity::context(Context* context)
{
    this->__context = new Context(context);

    return this;
}

Context* Activity::context()
{
    return this->__context ?: (this->__context = new Context);
}

Activity::~Activity()
{
    this->stop();
    delete this->__context;
}