/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "task/task.hpp"

using namespace Archaic;

Task::Task(const Job &job)
{
    this->job = job;
}

bool Task::exec()
{
    if (!this->trigger("start")) {
        return false;
    }
    this->job();
    return this->trigger("stop");
}