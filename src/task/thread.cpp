/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <iostream>

#include "activity.hpp"
#include "task/thread.hpp"

using namespace Archaic;

void Thread::__exec()
{
    this->job();

    this->activity->sync(this);
}

void Thread::init()
{
    this->on("start", [this] () {
        this->thread = new std::thread(&Thread::__exec, this);
        return JobResult::Continue;
    })->on("stop", [this] () {
        this->join();
        return JobResult::Continue;
    });
}

Thread::Thread(const Job &func): Task(func) {
    this->thread = nullptr;
    this->activity = nullptr;
}

Thread::Thread(Activity *activity, const Job &func): Thread(func)
{
    this->activity = activity;
    this->init();
}

bool Thread::exec()
{
    this->activity->async(this);
    return true;
}

void Thread::join()
{
    if (this->thread && this->thread->joinable()) {
        this->thread->join();
        delete this->thread;
    }
}