/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "task/loop.hpp"

using namespace Archaic;

bool Loop::exec()
{
    if (!this->trigger("start")) {
        return false;
    }
    JobResult result;
    do {
        result = this->job();
    } while (!this->__done && result == JobResult::Continue);

    return this->trigger("stop");
}

bool Loop::done()
{
    return this->guard<bool>([this] () {
        return this->__done;
    });
}

Loop* Loop::done(bool val)
{
    this->vguard([this, &val] () {
        this->__done = val;
    });

    return this;
}
