/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "activity.hpp"
#include "task/async-loop.hpp"

using namespace Archaic;

void AsyncLoop::__exec()
{
    JobResult result;
    do {
        result = this->job();
    } while (!this->__done && result == JobResult::Continue);

    this->activity->sync(this);
}

bool AsyncLoop::done()
{
    return this->guard<bool>([this] () {
        return this->__done;
    });
}

AsyncLoop* AsyncLoop::done(bool val)
{
    this->vguard([this, &val] () {
        this->__done = val;
    });

    return this;
}

AsyncLoop::AsyncLoop(Activity *activity, const Job &func): Thread(func) {
    this->on("stop", [this] () {
        this->done(true);

        return JobResult::Continue;
    });
    this->activity = activity;
    this->init();
}
