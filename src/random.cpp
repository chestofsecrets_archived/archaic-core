/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <chrono>

#include "random.hpp"

using namespace Archaic;

std::mt19937 Random::generator((std::random_device())());

std::string Random::hex(unsigned blockNumber)
{
    const char alphabet[] = "0123456789abcdef";
    std::string result;
    result.reserve(blockNumber * 8);

    for (auto i = blockNumber; i > 0; i--) {
        uint64_t rand = Random::generator();

        result += static_cast<char>(alphabet[rand >> 7 & 15]);
        result += static_cast<char>(alphabet[rand >> 6 & 15]);
        result += static_cast<char>(alphabet[rand >> 5 & 15]);
        result += static_cast<char>(alphabet[rand >> 4 & 15]);
        result += static_cast<char>(alphabet[rand >> 3 & 15]);
        result += static_cast<char>(alphabet[rand >> 2 & 15]);
        result += static_cast<char>(alphabet[rand >> 1 & 15]);
        result += static_cast<char>(alphabet[rand & 15]);
    }

    return result;
}
