/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "debug/logger.hpp"

using namespace Archaic::Debug;

Logger::Logger(Archaic::Debug::LogProcessor *processor)
{
    this->logProcessor = processor;
}

Logger* Logger::fatal(const std::string &message)
{
    this->logProcessor->message(LogProcessor::Level::Fatal, message);

    return this;
}

Logger* Logger::error(const std::string &message)
{
    this->logProcessor->message(LogProcessor::Level::Error, message);

    return this;
}

Logger* Logger::warn(const std::string &message)
{
    this->logProcessor->message(LogProcessor::Level::Warning, message);

    return this;
}

Logger* Logger::info(const std::string &message)
{
    this->logProcessor->message(LogProcessor::Level::Info, message);

    return this;
}

Logger::~Logger()
{
    delete this->logProcessor;
}