/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <iostream>
#include <ctime>
#include <iomanip>

#include "debug/log-processor.hpp"

using namespace Archaic::Debug;


void LogProcessor::message(Archaic::Debug::LogProcessor::Level level, const std::string &message)
{
    auto time = std::time(nullptr);
    auto localTime = std::localtime(&time);

    std::cerr << std::put_time(localTime, "[%Y-%m-%d %H:%M:%S] ");

    switch (level) {
        case Level::Fatal:
            std::cerr << "(F) " << message << std::endl;
            exit(1);
        case Level::Error:
            std::cerr << "(E) ";
            break;
        case Level::Warning:
            std::cerr << "(W) ";
            break;
        case Level::Info:
            std::cerr << "(I) ";
            break;
        default:
            std::cerr << "(?) ";
            break;
    }

    std::cerr << message << std::endl;
}