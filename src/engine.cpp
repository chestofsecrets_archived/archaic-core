/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "context/context.hpp"
#include "context/selector/any-rule.hpp"
#include "context/selector/is-rule.hpp"
#include "context/selector/combine-rule.hpp"

#include "engine.hpp"

using namespace Archaic;

Engine* Engine::engine = nullptr;
unsigned Engine::started = 0;

Engine::Engine()
{
    if (Engine::started++) {
        Engine::started--;

        this->logger()->warn("The engine has already been started.");
    }

    Rule::apply<AnyRule>("any");
    Rule::apply<IsRule>("is");

    Rule::apply<CombineRule>("combine");
    Rule::apply<CombineRule>("add");
    Rule::apply<CombineRule>("");

    Context::type<Context>("context");

    Engine::engine = this;
}

Engine* Engine::addActivity(Activity *activity)
{
    if (std::find(this->activities.begin(), this->activities.end(), activity) != this->activities.end()) {
        this->logger()->warn("An activity of name \"" + activity->name() + "\" is already registered.");
        return this;
    }

    activity->on("start", [this] () {
        Engine::started++;

        return JobResult::Continue;
    },"engine")->on("stop", [this, activity] () {
        Engine::started--;

        return JobResult::Continue;
    }, "engine");

    this->activities.push_back(activity);

    return this;
}

Engine* Engine::removeActivity(Activity *activity)
{
    activity->off("start", "engine")->off("stop", "engine");

    std::vector<Activity*>::iterator pos;

    while ((pos = std::find(this->activities.begin(), this->activities.end(), activity)) != this->activities.end()) {
        this->activities.erase(pos);
    }

    return this;
}

std::vector<Activity*> Engine::activity(const std::string &name)
{
    std::vector<Activity*> result;

    for (auto &activity : this->activities) {
        if (activity->name() == name) {
            result.push_back(activity);
        }
    }

    return result;
}

std::vector<Activity*> Engine::activity()
{
    return this->activities;
}

Engine* Engine::get()
{
    return Engine::engine;
}

Debug::Logger* Engine::logger()
{
    return this->log ?: this->log = new Debug::Logger(new Debug::LogProcessor);
}

Debug::Logger* Engine::logger(Debug::LogProcessor *processor)
{
    if (this->log) {
        this->log->warn("Logger is already initialized");
        return this->log;
    }
    return this->log = new Debug::Logger(processor);
}

Engine::~Engine()
{
    for (auto &context : this->contexts) {
        delete context;
    }
    for (auto &activity : this->activities) {
        delete activity;
    }
    delete this->log;

    Engine::started--;

    Engine::engine = nullptr;
}
