/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef CORE_STRING_HPP
#define CORE_STRING_HPP

#include <vector>
#include <string>
#include <functional>

namespace Archaic
{
    /**
     * Helper class to manage with strings.
     * Combining these methods may be inefficient as each of them iterates at least once through the string.
     */
    class String
    {
    public:

        /**
         * Explosion is used to tell explode function how to process current character:
         * Include: character must be appended to the piece
         * Exclude: character must be excluded
         * Next: character must be appended to the piece and processing of next piece must be started
         * Break: character must be excluded and processing of next piece must be started
         * Separate: character must be return as a separate piece and processing of next piece must be started
         * Stop: processing must be stopped, return what was processed
         * Halt: processing must be stopped and there must be nothing to return
         */
        enum class Explosion {
            Include,
            Exclude,
            Next,
            Break,
            Separate,
            Stop,
            Halt
        };

        /**
         * Explodes the string by a character delimiter
         * @param delimiter
         * @param string
         * @return
         */
        static std::vector<std::string> explode(const char &delimiter, const std::string &string);

        /**
         * Explodes the string by a string delimiter
         * @param delimiter
         * @param string
         * @return
         */
        static std::vector<std::string> explode(const std::string &delimiter, const std::string &string);

        /**
         * Explodes the string using custom delimiter rule
         * @param delimiter
         * @param string
         * @param skipEmpty
         * @return
         */
        static std::vector<std::string> explode(const std::function<Explosion(const char&)> &delimiter, const std::string &string, bool skipEmpty = false);

        /**
         * Breaks the string into an array of single character strings
         * @param string
         * @return
         */
        static std::vector<std::string> explode(const std::string &string);

        /**
         * Merges strings using character delimiter
         * @param delimiter
         * @param strings
         * @return
         */
        static std::string implode(const char &delimiter, const std::vector<std::string> &strings);

        /**
         * Merges strings using string delimiter
         * @param delimiter
         * @param strings
         * @return
         */
        static std::string implode(const std::string &delimiter, const std::vector<std::string> &strings);

        /**
         * Merges strings
         * @param strings
         * @return
         */
        static std::string implode(const std::vector<std::string> &strings);

        /**
         * Splits the string by regular expression.
         * Warning: using explode(const std::function<Explosion(const char&)>&, const std::string&) is approximately
         * 50 times faster than this method. Use it only if you really need regex
         *
         * @param pattern
         * @param string
         * @param skipEmpty
         * @return
         */
        static std::vector<std::string> preg_split(const std::string &pattern, const std::string &string, bool skipEmpty = false);

        /**
         * Removes all space characters (' ', \n, \f, \r, \t, \v) from start and end of the string
         * @param string
         * @return
         */
        static std::string trim(const std::string &string);
    };
}

#endif //CORE_STRING_HPP
