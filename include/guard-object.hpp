/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef CORE_GUARD_OBJECT
#define CORE_GUARD_OBJECT

#include <mutex>
#include <functional>

#include "object.hpp"

namespace Archaic
{
    /**
     * GuardObject must be used as an extension of Object when its needed
     * to work with its members in multiple threads simultaneously
     */
    class GuardObject : public Object
    {
        std::mutex mutex;
    public:
        GuardObject();

        /**
         * Locks execution of the function in current thread
         * @tparam T return type
         * @param func
         * @return T
         */
        template <typename T> T guard(const std::function<T()> &func);

        /**
         * Unlocks execution of the function in current thread
         * @tparam T return type
         * @param func
         * @return T
         */
        template <typename T> T loose(const std::function<T()> &func);

        /**
         * Locks execution of the void function in current thread
         * @param func
         */
        void vguard(const std::function<void()> &func);

        /**
         * Unlocks execution of the void function in current thread
         * @param func
         */
        void vloose(const std::function<void()> &func);
    };
}

#include "guard-object.inl"

#endif //CORE_GUARD_OBJECT
