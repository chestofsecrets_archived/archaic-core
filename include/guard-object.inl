/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef CORE_GUARD_OBJECT_INL
#define CORE_GUARD_OBJECT_INL

using namespace Archaic;

template <typename T>
T GuardObject::guard(const std::function<T()> &func)
{
    mutex.lock();
    auto result = func();
    mutex.unlock();

    return result;
}

template <typename T>
T GuardObject::loose(const std::function<T()> &func)
{
    mutex.unlock();
    auto result = func();
    mutex.lock();

    return result;
}

#endif //CORE_GUARD_OBJECT_INL
