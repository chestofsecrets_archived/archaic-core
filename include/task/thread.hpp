/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef CORE_THREAD
#define CORE_THREAD

#include <map>
#include <thread>
#include <functional>

#include "task.hpp"

namespace Archaic
{
    /**
     * Declared in activity.hpp
     */
    class Activity;

    /**
     * Class to create a task in a separate thread
     */
    class Thread : public Task
    {
        std::thread *thread;
        virtual void __exec();
        void join();
        friend class Activity;
    protected:
        void init();
        Activity *activity;
        Thread(const Job &func);
    public:

        /**
         * Initializes task with the given job and sets given activity as its owner
         * @param activity
         * @param func
         */
        Thread(Activity *activity, const Job &func);

        /**
         * Executes the task
         * @return true
         */
        bool exec() override;
    };
}

#endif //CORE_THREAD
