/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef CORE_TASK
#define CORE_TASK

#include <functional>

#include "../event-object.hpp"

namespace Archaic
{
    /**
     * Class to create linear task in current thread
     */
    class Task : public EventObject
    {
    protected:
        Job job;
    public:
        /**
         * Initializes task with the given job
         * @param job
         */
        explicit Task(const Job &job);

        /**
         * Executes the task
         * @return false if the task was canceled on start or stop. Else true
         */
        virtual bool exec();
    };
}

#endif //CORE_TASK
