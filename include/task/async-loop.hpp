/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef CORE_ASYNC_LOOP
#define CORE_ASYNC_LOOP

#include "thread.hpp"

namespace Archaic
{
    /**
     * Class to create looping task in a separate thread
     */
    class AsyncLoop : public Thread
    {
        bool __done = false;
        void __exec() override;
    public:
        /**
         * @return loop completion status
         */
        bool done();

        /**
         * Changes loop completion status
         * @param val
         * @return self as AsyncLoop class object
         */
        AsyncLoop* done(bool val);

        /**
         * Initializes loop with the given job and sets given activity as its owner
         * @param activity
         * @param func
         */
        AsyncLoop(Activity *activity, const Job &func);
    };
}

#endif //CORE_ASYNC_LOOP
