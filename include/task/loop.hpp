/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef CORE_LOOP_HPP
#define CORE_LOOP_HPP

#include "task.hpp"

namespace Archaic
{
    /**
     * Class to create looping task in current thread
     */
    class Loop : public Task
    {
        bool __done = false;
    public:
        /**
         * Executes the loop
         * @return false if the loop was canceled on start or stop. Else true
         */
        bool exec() override;

        /**
         * @return loop completion status
         */
        bool done();

        /**
         * Changes loop completion status
         * @param val
         * @return self as Loop object
         */
        Loop* done(bool val);
    };
}

#endif //CORE_LOOP_HPP
