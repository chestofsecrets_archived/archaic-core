/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef CORE_ENGINE_HPP
#define CORE_ENGINE_HPP

#include <vector>

#include "context/context-element.hpp"

#include "debug/logger.hpp"

#include "activity.hpp"

namespace Archaic {

    /**
     * General engine class. An instance of this class must be created and this instance must have at least one
     * Activity object where processing code must be described.
     */
    class Engine: public EventObject
    {
        friend Activity;

        static Engine *engine;
        static unsigned started;

        std::vector<Activity*> activities;
        std::vector<Context*> contexts;
        Debug::Logger *log = nullptr;

        /**
         * Registers a new activity with given name. If an Activity with this name already exists in
         * the Engine, it will be replaced with the given one and trigger a warning.
         * @warning Given activity is already registered
         * @param activity
         * @return
         */
        Engine* addActivity(Activity *activity);

        /**
         * Removes activity from engine
         * @param activity
         * @return
         */
        Engine* removeActivity(Activity *activity);
    public:
        /**
         * Initializes the engine
         * @fatal There already exists an instance of an Engine
         */
        Engine();

        /**
         * Retrieves an instance of an engine
         * @return
         */
        static Engine* get();

        /**
         * Retrieves a vector of activities with given name
         * @param name
         * @return
         */
        std::vector<Activity*> activity(const std::string &name);

        /**
         * Retrieves all activities
         * @param name
         * @return
         */
        std::vector<Activity*> activity();

        /**
         * Returns a Logger instance
         * @return
         */
        virtual Debug::Logger *logger();

        /**
         * Creates a logger instance with given processor
         * @warning Logger is already initialized
         * @return
         */
        virtual Debug::Logger *logger(Debug::LogProcessor *processor);

        /**
         * Frees the engine, its activities and contexts
         */
        virtual ~Engine();
    };
}

#endif //CORE_ENGINE_HPP
