/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef CORE_RANDOM_HPP
#define CORE_RANDOM_HPP

#include <string>
#include <random>

namespace Archaic
{
    /**
     * Helper class to generate random objects
     */
    struct Random
    {
        static std::mt19937 generator;
        /**
         * Generates a random hash of given number of blocks
         * One block equals 8 bytes
         * @param blockNumber
         * @return
         */
        static std::string hex(unsigned blockNumber = 1);
    };
}


#endif //CORE_RANDOM_HPP
