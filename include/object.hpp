/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef CORE_OBJECT
#define CORE_OBJECT

#include <string>
#include <vector>
#include <map>
#include <any>

namespace Archaic{

    class Object
    {
        std::vector<std::string> features;
    public:

        /**
         * @return full binary representation of the object parameters
         */
        virtual std::string toBinary();

        /**
         * @param params
         * @return binary representation of given object parameters
         */
        virtual std::string toBinary(const std::vector<std::string> &params);

        /**
         * Sets object parameters values according to the given ones
         * @param params
         */
        virtual void fromBinary(const std::map<std::string, std::any> &params);
    };
}

#endif //ARCHAIC_CORE_OBJECT
