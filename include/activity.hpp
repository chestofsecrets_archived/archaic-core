/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef CORE_ACTIVITY
#define CORE_ACTIVITY

#include <map>
#include <string>
#include <vector>
#include <thread>

#include "task/thread.hpp"

namespace Archaic
{
    /**
     * Declared in context.hpp
     */
    class Context;

    /**
     * An instance of Activity class must be used as a container of a processing code.
     * It owns all threads and its context which represents a window, activity or any other entity
     * that is used to render objects to the screen.
     */
    class Activity: public EventObject
    {
        std::vector<Thread*> threads;
        Context* __context = nullptr;
        bool started = false;
        std::string __name = "";
    public:
        Activity();

        /**
         * Sets a new task to start in separate thread to start when the activity is started.
         * If the activity is already running the task will start immediately.
         * @param task
         * @return Thread object of the task
         */
        Thread* async(Thread *task);

        /**
         * Sets a new task to start in separate thread to start when the activity is started.
         * If the activity is already running the task will start immediately.
         * @param func
         * @param loop
         * @return Thread object of the task
         */
        Thread* async(const Job& func, bool loop = false);

        /**
         * Joins and frees given thread
         * @param func
         * @return self as Activity
         */
        Activity* sync(Thread *func);

        /**
         * Sets a new name for the activity
         * @param name
         * @return nullptr if failed to change the name (ex. activity with given name already exists). Else self
         */
        Activity* name(const std::string &name);

        /**
         * @return name of the application
         */
        std::string& name();

        /**
         * Starts the activity. If none of the activities is marked as current, sets this as such
         * The activity is automatically assigned to the engine if it wasn't yet.
         * @fatal Engine wasn't started
         * @return false if the activity was canceled to start or stop. Else true
         */
        bool start();

        /**
         * Sets the stop signal to the activity loop
         * @return self as Activity
         */
        Activity* stop();

        /**
         * Sets a context to the activity
         * Warning: this method copies context element tree which will be inefficient if used frequently
         * @param context
         * @return
         */
        Activity* context(Context* context);

        /**
         * Returns current context of an activity
         * @return
         */
        Context* context();

        ~Activity();
    };
}

#endif //CORE_ACTIVITY
