/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef CORE_DEBUG_LOG_PROCESSOR_HPP
#define CORE_DEBUG_LOG_PROCESSOR_HPP

#include <string>

namespace Archaic::Debug
{
    /**
     * Class is used to output debug messages to stderr.
     * If you need to output messages somewhere else, you can extend this one
     * and initialize Engine Logger with your extension
     */
    class LogProcessor
    {
    public:
        enum class Level {
            Fatal,
            Error,
            Warning,
            Info
        };
        void message(Level level, const std::string &message);
    };
}

#endif //CORE_DEBUG_LOG_PROCESSOR_HPP
