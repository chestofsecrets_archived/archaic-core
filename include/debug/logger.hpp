/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef CORE_DEBUG_LOG_HPP
#define CORE_DEBUG_LOG_HPP

#include "log-processor.hpp"

namespace Archaic::Debug
{
    /**
     * Class is used as an interface to LogProcessor
     */
    class Logger
    {
        LogProcessor *logProcessor;
    public:
        explicit Logger(LogProcessor* processor);
        Logger* fatal(const std::string &message);
        Logger* error(const std::string &message);
        Logger* warn(const std::string &message);
        Logger* info(const std::string &message);
        ~Logger();
    };
}

#endif //CORE_DEBUG_LOG_HPP
