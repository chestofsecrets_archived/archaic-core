/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef CORE_EVENT_OBJECT
#define CORE_EVENT_OBJECT

#include <functional>
#include <vector>
#include <string>
#include <map>
#include <thread>

#include "guard-object.hpp"

namespace Archaic
{
    /**
     * Return type for all jobs completed.
     *
     * Stop must be called when the job procession must be aborted.
     * NextGroup must be called when the event listening by the current group must be aborted.
     * Continue must be called in any other case.
     */
    enum class JobResult {Continue, NextGroup, Stop};
    using Job = std::function<JobResult()>;
    using JobGroup = std::vector<Job>;
    using JobType = std::map<std::string, JobGroup>;

    class EventObject: public GuardObject
    {
    protected:
        std::map<std::string, JobType> eventTypes;
    public:
        EventObject();

        /**
         * Creates new event listener
         * @param eventName
         * @param listener
         * @param groupName
         * @return self as EventObject
         */
        EventObject* on(const std::string &eventName, const Job &listener, const std::string &groupName = "");

        /**
         * Creates new event listener that will be fired only once
         * @param eventName
         * @param listener
         * @param groupName
         * @return name of the "group" that the event listener was registered into
         */
        std::string once(const std::string &eventName, const Job &listener, const std::string &groupName = "");

        /**
         * Disables event listeners
         * @param eventName
         * @param groupName if specified, disables only event listeners of this group
         * @return self as EventObject
         */
        EventObject* off(const std::string &eventName, const std::string &groupName = "");

        /**
         * Counts event listeners
         * @param eventName if specified, counts only event listeners of this event
         * @param groupName if specified, counts only event listeners of this group
         * @return Number of event listeners
         */
        unsigned long listeners(const std::string &eventName = "", const std::string &groupName = "");

        /**
         * Triggers an event
         * @return false if it was aborted. Else true
         */
        bool trigger(const std::string &eventName);

        /**
         * Creates a "message" event listener which will trigger the event with given name when triggered
         * @param eventName
         * @param groupName
         * @return name of the "group" that the event listener was registered into
         */
        std::string message(const std::string &eventName, const std::string &groupName = "");

        /**
         * Creates a "message" event listener which will execute the job when triggered
         * @param job
         * @param groupName
         * @return name of the "group" that the event listener was registered into
         */
        std::string message(const Job &job, const std::string &groupName = "");

        /**
         * Disables "message" event of given groupName
         * @param eventName
         * @param groupName
         * @return self as EventObject
         */
        EventObject* shush(const std::string &groupName = "");

        /**
         * Triggers all "message" event listeners
         * @return false if it was aborted. Else true
         */
        bool listen();
    };
}

#endif //CORE_EVENT_OBJECT
