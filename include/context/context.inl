/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef CORE_CONTEXT_INL
#define CORE_CONTEXT_INL

#include "../engine.hpp"

using namespace Archaic;

template <typename T>
ContextElement* Context::ElementBuilder::build(){
    return new T;
}

template <typename T>
void Context::type(const std::string &name)
{
    auto pos = Context::builders.find(name);

    if (pos != Context::builders.end()) {
        auto engine = Engine::get();

        if (engine) {
            engine->logger()->warn("Element type \"" + name + "\" is already registered");
        } else {
            Debug::LogProcessor proc;
            Debug::Logger(&proc).warn("Element type \"" + name + "\" is already registered");
        }
    }

    Context::builders[name] = ElementBuilder::build<T>;
}

#endif //CORE_CONTEXT_INL
