/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef CORE_CONTEXT
#define CORE_CONTEXT

#include <vector>

#include "context-element.hpp"

namespace Archaic
{
    /**
     * Context class is used to contain a tree of elements to visually represent Activity
     */
    class Context: public ContextElement
    {
        class ElementBuilder
        {
        public:
            template <typename T>
            static ContextElement* build();
        };
        static std::map<std::string, std::function<ContextElement*()>> builders;
    public:
        Context();
        explicit Context(Context *context);

        /**
         * Registers new type
         * @warning Type is already registered
         * @tparam T
         * @param name
         */
        template <typename T>
        static void type(const std::string &name);

        /**
         * Creates an element of given type
         * @warning Type is not registered, element is created of type ContextElement instead
         * @param name Name of element type
         * @return
         */
        static ContextElement* build(const std::string &name);
    };
}

#include "context.inl"

#endif //CORE_CONTEXT
