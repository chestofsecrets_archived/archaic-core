/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef CORE_CONTEXT_ELEMENT
#define CORE_CONTEXT_ELEMENT

#include "../random.hpp"
#include "../event-object.hpp"

namespace Archaic
{
    /**
     * Declared in context/context.hpp
     */
    class Context;

    /**
     * ContextElement is the EventObject that is contained inside the Context.
     * It has a unique id that is used to identify the Object while not having it's instance.
     */
    class ContextElement: public EventObject
    {
        friend Context;

        /**
         * These constructors are used internally to create elements' appropriate class instance by its type.
         * Use Context::type to register a type and Context::build to create an instance of an element instead.
         */
        ContextElement() = default;
        explicit ContextElement(const std::string &type);
        ContextElement* type(const std::string &type);
    protected:
        std::string __type;
        std::string __id = Random::hex();
        std::vector<std::string> classList;
        ContextElement *__parent = nullptr;
        std::vector<ContextElement*> __children;
    public:
        /**
         * Copies an element and its children
         * @param element
         */
        explicit ContextElement(ContextElement* element);

        /**
         * Copies an element and its children and optionally events
         * @param element
         * @param passEvents
         */
        ContextElement(ContextElement* element, bool passEvents);

        /**
         * Retrieves closest element's parent
         * @return
         */
        ContextElement* parent();

        /**
         * Retrieves all element's parents
         * @return
         */
        std::vector<ContextElement*> parents();

        /**
         * Retrieves element's children
         * @param deep Searches deeply for children, if true
         * @return
         */
        std::vector<ContextElement*> children(bool deep = false);

        /**
         * Appends an element to the current one
         * @param child
         * @return
         */
        ContextElement* append(ContextElement* child);

        /**
         * Returns element id
         * @return
         */
        std::string id();

        /**
         * Sets element id
         * @param id
         * @return
         */
        ContextElement* id(const std::string &id);

        /**
         * Returns element type
         * @return
         */
        std::string type();

        /**
         * Checks if element has given class
         * @param className
         * @return
         */
        bool hasClass(const std::string &className);

        /**
         * Appends a class to the element
         * @param className
         * @return
         */
        ContextElement* addClass(const std::string &className);

        /**
         * Removes a class from the element
         * @param className
         * @return
         */
        ContextElement* removeClass(const std::string &className);

        /**
         * Returns all element classes
         * @return
         */
        std::vector<std::string> getClasses();

        /**
         * Frees the element and its children
         */
        ~ContextElement();
    };
}


#endif //CORE_CONTEXT_ELEMENT
