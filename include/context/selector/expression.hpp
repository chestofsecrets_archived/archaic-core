/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef CORE_RULE_PARSER
#define CORE_RULE_PARSER

#include "rule.hpp"

/**
 * Expression is a class which used for searching through context element tree
 * for a target element using Archaic Selector Language.
 *
 * It is recommended to create one instance of the expression to search multiple times with
 * so ASL expression is not interpreted every time you need it.
 *
 * ASL is similar to CSS in some ways. Here is the list of operators of ASL:
 * > - moves to children of element(s)
 * < - moves to parents of element(s)
 * ~ - use deep search for children (parents)
 * . - filter by class
 * # - filter by id (faster than by class as it returns only first one element to match)
 * ! - invert next statement
 * : - use a custom rule
 *
 * Expressions may be divided by , or ;
 *
 * Custom rule is an extension of abstract class Rule which is used to process input element by its own logic.
 * One can pass arguments to a custom rule in [] dividing them by ,
 * Custom rule should be registered on engine start using static Rule::apply method
 * There are two standard rules: :is (implemented in IsRule) and AnyRule (implemented in AnyRule).
 * :is returns all context elements that satisfy all its conditions described in arguments
 * :any returns all context elements that satisfy any of its conditions
 *
 * Examples:
 * context > panel -- searches for all panels placed directly on current element which must be of type context
 * > panel -- same as above but current element may be of any type. Not that no asterisk is needed
 * , > panel -- returns all panels on a current element and the element itself
 * ~> button -- searches for all buttons
 * ~> button.red -- searches for all red buttons
 * ~> button!.red - searches for all not red buttons
 * context > panel:is[~>button] -- searches for all panels in a context which contain buttons
 * ~> :any[label.red, button:any[.black, .bold, .lime!:is[.bold]]] - searches for all red labels, black or bold buttons
 *     and lime but not bold buttons
 * ~> button#exit - searches for an exit button implying that there is only one
 * ~> panel:is[>label, >button] - searches for all panels which contain labels and buttons
 */

namespace Archaic::Selector
{
    class Expression
    {
        struct Operator {
            static const char DeepFlag;
            static const char Child;
            static const char Parent;
            static const char Class;
            static const char Id;
            static const char Rule;
            static const char InvertFlag;
            static const char StatementSeparator;
            static const char ArgumentSeparator;
            static const char ListBegin;
            static const char ListEnd;
            static const char Any;
        };
        /**
         * Statement structure is used to contain a parsed statement and related rules
         */
        struct Statement
        {
            std::vector<std::pair<char, std::vector<std::pair<char, std::string>>>> statement;
            std::map<unsigned long, Rule*> rules;
        };

        std::vector<Statement*> statements;
    public:

        /**
         * Parses new Expression
         * @param expression ASL representation of the expression
         */

        explicit Expression(const std::string &expression);

        /**
         * Processes the expression
         * @param element Element to process expression with
         * @return Vector of elements gotten by processing the expression
         */

        std::vector<ContextElement*> exec(ContextElement* element);

        /**
         * Frees parsed expression
         */

        ~Expression();
    };
}

#endif //CORE_RULE_PARSER
