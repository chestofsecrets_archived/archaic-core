/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef CORE_ABSTRACT_RULE
#define CORE_ABSTRACT_RULE

#include <vector>
#include <string>

#include "../context-element.hpp"

namespace Archaic::Selector
{
    /**
     * Abstract class describing custom rule or processing the expression
     */

    class Rule
    {
    private:
        static std::map<std::string, std::function<Rule*(const std::vector<std::string>&)>> rules;
    public:

        /**
         * Associates Rule class extension with its string name.
         * Association fails if there already is a rule with given name
         *
         * @tparam T Class-extension of Rule
         * @param ruleName Name of a rule
         * @return Result of association
         */

        template <typename T, typename std::enable_if<std::is_base_of<Rule, T>::value>::type* = nullptr>
        static bool apply(const std::string &ruleName);

        /**
         * Cancels the association of a rule with its string name
         * @param ruleName Name of a rule
         * @return true if there was a rule to remove association of
         */

        static bool cancel(const std::string &ruleName);

        /**
         * Initializes a rule of given name with arguments
         * @param ruleName Name of a rule
         * @param args Arguments of a rule
         * @return Rule object to test with
         */

        static Rule* create(const std::string &ruleName, const std::vector<std::string> &args);

        /**
         * Parses arguments
         * @param args
         */

        explicit Rule(const std::vector<std::string> &args);

        /**
         * Tests an element
         * @param element Element to test
         * @param inverted Flag which determines if the rule logic must be inverted
         * @return
         */

        virtual std::vector<ContextElement*> test(ContextElement* element, bool inverted) = 0;

        /**
         * Frees parsed rule
         */

        virtual ~Rule() = default;
    };
}

#include "rule.inl"

#endif //CORE_ABSTRACT_RULE
