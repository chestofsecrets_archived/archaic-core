/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef CORE_ANY_RULE_HPP
#define CORE_ANY_RULE_HPP

#include "rule.hpp"
#include "expression.hpp"

namespace Archaic::Selector
{
    /**
     * Checks if an element satisfies any expression represented in its arguments
     *
     * @assoc :any
     */

    class AnyRule : public Rule {
        std::vector<Expression*> expressions;
    public:
        explicit AnyRule(const std::vector<std::string> &args);
        std::vector<ContextElement*> test(ContextElement *element, bool inverted) override;
        ~AnyRule() override;
    };
}

#endif //CORE_ANY_RULE_HPP
