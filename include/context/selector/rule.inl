/**
 * @project Archaic
 * @module Core
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-core/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef CORE_RULE_INL
#define CORE_RULE_INL

#include <type_traits>

using namespace Archaic::Selector;

template <typename T, typename std::enable_if<std::is_base_of<Rule, T>::value>::type*>
bool Rule::apply(const std::string &ruleName)
{
    auto position = Rule::rules.find(ruleName);

    if (position != Rule::rules.end()) {
        return false;
    }

    Rule::rules.emplace(ruleName, [] (const std::vector<std::string> &arguments) {
        return new T(arguments);
    });

    return true;
}

#endif //CORE_RULE_INL
